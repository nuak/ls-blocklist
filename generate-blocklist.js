const https = require("https");
const fs = require("fs");

const HOSTS_URL = "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts";
const DOMAINS_PER_FILE = 15000;

https
  .get(HOSTS_URL, res => {
    let stream = "";

    res.on("data", chunk => {
      stream += chunk;
    });

    res.on("end", () => {
      const domains = [
        ...new Set(
          stream
            .toString("utf8")
            .split("\n")
            .flat()
            .filter(line => line.startsWith("0.0.0.0") && !line.endsWith("0.0.0.0"))
            .map(line => line.split(" ")[1])
        )
      ];

      const domainChunks = domains.sort().reduce((chunks, domain, index) => {
        const chunkIndex = Math.floor(index / DOMAINS_PER_FILE);
        if (!chunks[chunkIndex]) chunks[chunkIndex] = [];
        chunks[chunkIndex].push(domain);
        return chunks;
      }, []);

      domainChunks.forEach((chunkDomains, i) => {
        fs.writeFileSync(
          `blocklist${i + 1}.lsrules`,
          JSON.stringify(
            {
              description: `Blocklist based on StevenBlack hosts (${domains.length} unique domains blocked)`,
              name: `Blocklist (${i + 1}/${domainChunks.length})`,
              rules: [
                {
                  action: "deny",
                  owner: "me",
                  process: "any",
                  "remote-domains": chunkDomains
                }
              ]
            },
            null,
            2
          ),
          "utf8"
        );
      });
    });
  })
  .on("error", e => {
    console.error(e);
  });
